#!/usr/bin/env bash
#
# Download and install Operator SDK binaries.
#
# Set the download variables explicitly.
RELEASE_VERSION=v1.21.0
SYSTEM=linux
ARCH=amd64

# Announce.
echo "Downloading OperatorSDK ${RELEASE_VERSION} for ${SYSTEM}_${ARCH}..."

# Linux download packages
for op in operator-sdk ansible-operator helm-operator; do
    curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/${op}_${SYSTEM}_${ARCH}
done

# Linux verify binary 
curl -L https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/checksums.txt | \
    grep _${SYSTEM}_${ARCH} > checksums.txt

sha256sum -c checksums.txt && echo "Downloads OK, deploying..." || (echo "Error downloading, aborting." && exit 1)
rm -f checksums.txt

# Install on Linux
sudo mkdir -p /usr/local/bin
for op in operator-sdk ansible-operator helm-operator; do
    sudo cp ${op}_${SYSTEM}_${ARCH} /usr/local/bin/${op}
    sudo chmod 755 /usr/local/bin/${op}
    rm -f ${op}_${SYSTEM}_${ARCH}
done

echo "Done!"

# End of install_operator.sh
