# myapp/models.py

from myapp import mydb

class User(mydb.Model):
    id = mydb.Column(mydb.Integer, primary_key=True, autoincrement=True)
    name = mydb.Column(mydb.String(64), index=True)
    eventname = mydb.Column(mydb.String(120))

    def __repr__(self):
        return '<{}> : <User {}>'.format(self.id, self.name)


    def showevent(self):
        return 'User {} at {}'.format(self.name, self.eventname)
